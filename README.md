# ShockBot

**The ShockBot is meant to be basic tool for quick cheap robotic projects.** 

## Description
This robot is going to be a ground mobile robot which is drived by a flywheel. 

A microcontroller will get the IMU data to automatically control the velocity of the flywheel and in this way get the goal heading.

![Hand Sketch of ShockBot ](illustrations/hand_sketch.jpeg)

## Support
Please feel free to use the gitlab issue tracker.

## Roadmap
The initial step is a prove of concept.

## Contributing
Anyone can fork this project and carry on new goals.

## Authors and acknowledgment
The main author of the project is Enrique Paredes Sánchez.

## License
Open project, free to use, no mention of the author needed.

## Project status
In developping.
