# Etapas del Proyecto

##  1 Planificación, definición de objetivos y requisitos

##  2 Revisión bibliográfica

##  3 Definición de hitos, tareas y planificación temporal 

##  4 Descripción y desarrollo de tests

##  5 Obtención del prototipo final

##  6 Demostraciones y validación

##  7 Elaboración de informes
